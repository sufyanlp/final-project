<?php
    include('connect.php'); 
    $sql = "SELECT * FROM major";
    $query = mysqli_query($connect,$sql);


    if (isset($_GET['status'])) {
        if ($_GET['status'] == 'fail') {
            $message = "Student ID already exists";
        }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Register Forms</title>

    <!-- Icons font CSS-->
    <link href="vendor1/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor1/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor1/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor1/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css1/main.css" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-01 p-t-180 p-b-100 font-poppins">
        <div class="wrapper wrapper--w780">
            <div class="card card-3">
                <div class="card-heading"> </div>
                <div class="card-body">
                    <h2 class="title">Registration Info</h2>
                    <form action="register_db.php" method="post">
                        <div class="input-group">
                            <input class="input--style-3" type="text" placeholder="Student-ID" name="id" id="id" required>
                            <?php if (isset($message)) {
                                            echo "<script type='text/javascript'>alert('$message');</script>";
 
                            } ?>
                        </div>
                        <div class="input-group">
                            <input class="input--style-3" type="password" placeholder="Password" name="password1" required>
                        </div>
                        <div class="input-group">
                            <div class="rs-select2 js-select-simple select--no-search">
                                <select name="major">

                                    <?php
                                    while ($result = mysqli_fetch_assoc($query)) {
                                    ?>
                                    <option value="<?php echo $result['major'] ?>"> <?php echo $result['major'] ?></option>
                                    <?php
                                    }
                                    ?>

                                </select>
                                <div class="select-dropdown"></div>
                            </div>
                        </div>
                        <div class="input-group">
                            <input class="input--style-3" type="email" placeholder="Email" name="email" required>
                        </div>
                        <div class="input-group">
                            <input class="input--style-3" type="text" placeholder="First Name" name="fname"required>
                        </div>
                        <div class="input-group">
                            <input class="input--style-3" type="text" placeholder="Last Name" name="lname"required>
                        </div>
                        <div class="p-t-10">
                            <input class="btn btn--pill btn--green" type="submit" name="submit">
                        </div>
                        
                    </form>
                    
                    <br>
                    <br>
                    
                    <div class="form-group text-center">
                        <div class="p-t-10">
                            <center><buttom class="btn btn--pill btn--frame" type="submit"><a href="login.php">Login</a></center>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor1/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor1/select2/select2.min.js"></script>
    <script src="vendor1/datepicker/moment.min.js"></script>
    <script src="vendor1/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js1/global.js"></script>

</body>
<!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->