<?php
    session_start();
    require "connect.php";
    $sql = "SELECT * FROM subjects WHERE category = 'free'";
    mysqli_set_charset($connect,'utf8');
    $query = mysqli_query($connect,$sql);
    $std = $_SESSION["studentid"];
    if (!isset($_SESSION["studentid"])) {
        header("location:login.php");
    }


?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">

    <title>RP System-Subject</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-style.css">
    <link rel="stylesheet" href="assets/css/owl.css">
    <style>
        
    .subject-card {
        padding-bottom: 50px;
    }

    </style>

</head>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <div class="logo">
                        <a href="home.php">RP System</a>
                    </div>
                </header>

                <!-- Page Heading -->
                <div class="page-heading">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <h1>Free Subject</h1><br/>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="container">
                    
                </div> -->


                <div class="container mt-5">

                    <!--Section: Content-->
                    <section>

                        <!-- Grid row -->
                        <div class="row">


                        <?php
                        while ($result = mysqli_fetch_assoc($query)) {
                        ?>
                        
                            <!-- Grid column -->
                            <div class="col-lg-4 col-md-12 mb-lg-0 mb-4 subject-card">

                                <!-- Card -->
                                <div class="card hoverable">

                                    <!-- Card image -->
                                    <img class="card-img-top" src="img/<?php echo $result['pic']?>" alt="Card image cap">

                                    <!-- Card content -->
                                    <div class="card-body">

                                        <!-- Title -->
                                        <h5 class="black-text"><?php echo $result['subjectid'];?></h5>
                                        <!-- Text -->
                                        <p class="card-title text-muted font-small mt-3 mb-2">
                                            <?php echo $result['thname'];?>
                                            <?php echo $result['enname'];?>
                                        </p>

                                        <a href="subjectdatail.php?id=<?php echo $result['subjectid'];?>"><button type="button" class="btn btn-flat text-primary p-0 mx-0">Read more<i class="fa fa-angle-right ml-2"></i></button></a>

                                    </div>

                                </div>
                                <!-- Card -->

                            </div>
                            <!-- Grid column -->
                        <?php
                        }

                        ?>

                            


                        </div>
                        <!-- Grid row -->

                    

                    </section>
                    <!--Section: Content-->


                </div>

            </div>
        </div>

        <!-- Sidebar -->
        <div id="sidebar">

            <div class="inner">

                <!-- Search Box -->
                <section id="search" class="alt">
                    <form method="get" action="#">
                        <input type="text" name="search" id="search" placeholder="Search..." />
                    </form>
                </section>

                <!-- Menu -->
                <nav id="menu">
                    <ul>
                        <li><a href="home.php">HOME</a></li>

                        <li><a href="planing-before.php">Plan registration</a></li>

                        <li>
                            <span class="opener">Course</span>
                            <ul>
                                <li><a href="course-mkt.php">Marketing</a></li>
                                <li><a href="course-fin.php">Finance</a></li>
                                <li><a href="course-hrm.php">Human Resource Management</a></li>
                                <li><a href="course-bis.php">Business Information System</a></li>
                                <li><a href="course-lm.php">Logistics Management</a></li>
                                <li><a href="course-mice.php">Meeting Incentive Convention and Exhibition Management</a></li>
                                <li><a href="course-acc.php">Accounting</a></li>
                            </ul>
                        </li>

                        <li>
                            <span class="opener">Subject Category</span>
                            <ul>
                                <li><a href="subjectc-eng.php">English Subject</a></li>
                                <li><a href="subjectc-minor.php">Minor Subject</a></li>
                                <li><a href="subjectc-free.php">Free Subject</a></li>
                                <li><a href="subjectc-aas.php">Aesthetics and sports</a></li>
                                <li><a href="subjectc-main.php">Major Subject</a></li>
                            </ul>
                        </li>

                        <li><a href="profile.php">Profile</a></li>

                        <li><a href="logout.php">Logout</a></li>

                    </ul>
                </nav>

                <!-- Featured Posts -->
                <div class="featured-posts">
                    <div class="heading">
                        <h2>Developer</h2>
                    </div>
                    <div class="owl-carousel owl-theme">
                        <div class="featured-item">
                            <img src="pic-admin/sho.jpg" alt="featured one">
                            <p>CHANATIP SRISAI 6010513003</p>
                        </div>
                        </a>
                        <div class="featured-item">
                            <img src="pic-admin/frame.jpg" alt="featured two">
                            <p>KORAWIT BORAPAK 6010513039</p>
                        </div>
                        </a>
                        <div class="featured-item">
                            <img src="pic-admin/peat.jpg" alt="featured three">
                            <p>CHAYANON BOONCHUAI 6010513043</p>
                        </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Scripts -->
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/transition.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/custom.js"></script>
</body>


</body>

</html>