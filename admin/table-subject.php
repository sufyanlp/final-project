<?php 
    session_start();
    include('../connect.php'); 
    $sql = "SELECT * FROM subjects ";
    $query = mysqli_query($connect,$sql);
?>


<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Free Bootstrap Admin Template : Binary Admin</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->

    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TABLE STYLES-->
    <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

    <style>
    img {
        border: 1px solid #ddd;
        border-radius: 4px;
        padding: 5px;
        width: 150px;
    }
    </style>


</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Binary admin</a>
            </div>

            <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;"> Last access : 30 May
                2014 &nbsp; <a href="../logout.php" class="btn btn-danger square-btn-adjust">Logout</a>
            </div>
        </nav>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li class="text-center">
                        <img src="assets/img/find_user.png" class="user-image img-responsive" />
                    </li>
                    <li>
                        <a href="admin-index.php"><i class="fa fa-dashboard fa-3x"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="ui.html"><i class="fa fa-desktop fa-3x"></i> UI Elements</a>
                    </li>
                    <li>
                        <a href="table-user.php"><i class="fa fa-users fa-3x"></i> Users</a>
                    </li>
                    <li>
                        <a href="table-subject.php"><i class="fa fa-book fa-3x"></i> Subjects</a>
                    </li>
                    <li>
                        <a href="table-semester.php"><i class="fa fa-file fa-3x"></i> Semester </a>
                    </li>
                    <li>
                        <a href="table-comment.php"><i class="fa fa-comments fa-3x"></i> Comments </a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <!-- <div class="container"> -->
                <div class="row">
                    <div class="col-md-12">
                        <h2>Forms Page</h2>
                        <h5>Welcome Jhon Deo , Love to see you back. </h5>

                    </div>
                    <div class="col-md-12" style="text-align: right">
                        <a href="insert-subject.php" class="btn btn-success">Create Subject</a>
                    </div>


                </div>
                <!-- </div> -->
                <!-- /. ROW  -->
                <hr />

                <div class="row">
                    <div class="col-md-12">
                        <!-- Advanced Tables -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Advanced Tables
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover"
                                        id="dataTables-example">
                                        <!-- <img src="../images/avatar2.png"> -->
                                        <thead>
                                            <tr>
                                                <th>SUBJECT_ID</th>
                                                <th>TH_NAME</th>
                                                <th>ENG_NAME</th>
                                                <th>CATEGORY</th>
                                                <th>credit</th>
                                                <th>pic</th>
                                                <th>EDIT</th>
                                                <th>DELETE</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            while ($result = mysqli_fetch_assoc($query)) {
                                            ?>
                                            <tr class="odd gradeX">
                                                <td><?php echo $result['subjectid'];?></td>
                                                <td><?php echo $result['thname'];?></td>
                                                <td><?php echo $result['enname'];?></td>
                                                <td class="center"><?php echo $result['category'];?></td>
                                                <td class="center"><?php echo $result['credit'];?></td>
                                                <td class="center"><img src="../img/<?php echo $result['pic']?>"></td>
                                                <td><a href="edit-subject.php?id=<?php echo $result['subjectid'];?>"><button
                                                            class="btn btn-primary"><i class="fa fa-edit "></i>
                                                            Edit</button></a></td>
                                                <td><a href="del-sub.php?id=<?php echo $result['subjectid'] ?>"><button
                                                            class="btn btn-danger"><i class="fa fa-pencil"></i>
                                                            Delete</button></a></td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>

                                    </table>

                                </div>
                            </div>
                        </div>
                        <!--End Advanced Tables -->
                    </div>
                </div>

            </div>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
    <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- DATA TABLE SCRIPTS -->
    <script src="assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>



</body>

</html>