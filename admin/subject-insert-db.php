<?php 
    session_start();
    include('../connect.php');
    $subid = $_POST['subid'];
    $thname = $_POST['thname'];
    $enname = $_POST['enname'];
    $detail = $_POST['detail'];
    $category = $_POST['category'];
    $credit = $_POST['credit'];

    $target_dir = "../img/";
    $file_name = basename($_FILES["subject_photo"]["name"]);
    $target_file = $target_dir . $file_name;
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES["subject_photo"]["tmp_name"]);
    if($check === false) {
        echo "File is not an image.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
        exit();
    // if everything is ok, try to upload file
    } else {
        if (!move_uploaded_file($_FILES["subject_photo"]["tmp_name"], $target_file)) {
            echo "Sorry, there was an error uploading your file.";
            exit();
        }
    }

    $sql = "INSERT INTO subjects (subjectid, thname, enname, detail, category, credit, pic) VALUES ('$subid', '$thname','$enname', '$detail', '$category', '$credit', '$file_name')";
    
    // $sql2 = "INSERT INTO semesters (subjectsid, majorsid, semesterss, yearss) VALUES ('$std', '$pwd','$fname', '$lname', '$email', '$major', '$status')";
    
    // printf($sql);
    // exit();
    if (mysqli_query($connect, $sql)) {

        header("location:table-subject.php?status=pass");

    }else{

        header("location:edit-subject.php?status=fail");
    }

?>