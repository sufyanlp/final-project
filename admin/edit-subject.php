<?php 
    session_start();
    include('../connect.php'); 
    $id=$_GET["id"];
    $sql = "SELECT * FROM subjects WHERE subjectid = '$id'";
    mysqli_set_charset($connect,'utf8');
    $query = mysqli_query($connect,$sql);

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Free Bootstrap Admin Template : Binary Admin</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <style>
    img {
        border: 1px solid #ddd;
        border-radius: 4px;
        padding: 5px;
        width: 150px;
    }
    </style>
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Binary admin</a>
            </div>
            <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;"> Last access : 30 May
                2014 &nbsp; <a href="../logout.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li class="text-center">
                        <img src="assets/img/find_user.png" class="user-image img-responsive" />
                    </li>
                    <li>
                        <a href="admin-index.php"><i class="fa fa-dashboard fa-3x"></i> Admin Dashboard</a>
                    </li>
                    <li>
                        <a href="ui.html"><i class="fa fa-desktop fa-3x"></i> UI Elements</a>
                    </li>
                    <li>
                        <a href="table-user.php"><i class="fa fa-users fa-3x"></i> Users</a>
                    </li>
                    <li>
                        <a href="table-subject.php"><i class="fa fa-book fa-3x"></i> Subjects</a>
                    </li>
                    <li>
                        <a href="table-semester.php"><i class="fa fa-file fa-3x"></i> Semester </a>
                    </li>
                    <li>
                        <a href="table-comment.php"><i class="fa fa-comments fa-3x"></i> Comments </a>
                    </li>
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Forms Page</h2>
                        <h5>Welcome Jhon Deo , Love to see you back. </h5>

                    </div>
                </div>
                <!-- /. ROW  -->
                <hr />
                <div class="row">
                    <div class="col-md-12">
                        <!-- Form Elements -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Form Element Examples
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form role="form" method="post" action="processedit-sub.php"
                                            enctype="multipart/form-data">
                                            <?php
                                        while ($result = mysqli_fetch_assoc($query)) {
                                        ?>
                                            <input type="text" name="sid" value="<?php echo $result['subjectid']; ?>"
                                                hidden>

                                            <div class="form-group">
                                                <label>SUBJECT_ID</label>
                                                <input class="form-control" name="subid"
                                                    value="<?php echo $result['subjectid']; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>THAI_NAME</label>
                                                <input class="form-control" name="thname"
                                                    value="<?php echo $result['thname']; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>ENGLISH_NAME</label>
                                                <input class="form-control" name="enname"
                                                    value="<?php echo $result['enname']; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label>DETAIL</label>
                                                <textarea class="form-control" rows="3"
                                                    name="detail"><?php echo $result['detail']; ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>SELECT CATEGORY</label>
                                                <select class="form-control" name="category">
                                                    <option value="main">MAIN</option>
                                                    <option value="minor">MINOR</option>
                                                    <option value="free">FREE</option>
                                                    <option value="eng">ENG</option>
                                                    <option value="aas">AAS</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>SELECT CREDIT</label>
                                                <select class="form-control" name="credit">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>File input</label>
                                                <br>
                                                <label><img src="../img/<?php echo $result['pic']?>"></label>
                                                <input type="file" name="subject_photo" />
                                            </div>

                                            <?php
                                        } 
                                        ?>
                                            <button type="submit" class="btn btn-default">Submit </button>
                                            <button type="reset" class="btn btn-primary">Reset </button>
                                        </form>

                                    </div>


                                    <!-- /. PAGE INNER  -->
                                </div>
                                <!-- /. PAGE WRAPPER  -->
                            </div>
                            <!-- /. WRAPPER  -->
                            <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
                            <!-- JQUERY SCRIPTS -->
                            <script src="assets/js/jquery-1.10.2.js"></script>
                            <!-- BOOTSTRAP SCRIPTS -->
                            <script src="assets/js/bootstrap.min.js"></script>
                            <!-- METISMENU SCRIPTS -->
                            <script src="assets/js/jquery.metisMenu.js"></script>
                            <!-- CUSTOM SCRIPTS -->
                            <script src="assets/js/custom.js"></script>


</body>

</html>