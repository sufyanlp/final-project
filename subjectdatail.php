<?php
    session_start();
    include('connect.php');
    $std = $_SESSION["studentid"];
    $id=$_GET["id"];
    $sql = "SELECT * FROM subjects WHERE subjectid = '$id'";
    mysqli_set_charset($connect,'utf8');
    $query = mysqli_query($connect,$sql);
    $sql_comment = "SELECT * FROM comments RIGHT JOIN users ON comments.studentid = users.studentid WHERE subjectid = '$id'";
    $query_comments = mysqli_query($connect,$sql_comment);

    if (isset($_POST['btnSave']))
    {
        $txt = $_POST['main-commen'];
        $SQL = "INSERT INTO comments (commentid, subjectid, studentid,txt) VALUES ('', '$id' ,'$std', '$txt')";
        $result = mysqli_query($connect,$SQL);
        header("location.reload()");
    }
    
    if (!isset($_SESSION["studentid"])) {
        header("location:login.php");
    }


?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>RP System Detail</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-style.css">
    <link rel="stylesheet" href="assets/css/owl.css">

    <!-- <link rel="stylesheet" href="css/reset.css" />
    <link rel="stylesheet" href="css/style.css" /> -->
    <style>
    /* CSS Test begin */
    .comment-box {
        margin-top: 30px !important;
    }
    /* CSS Test end */

    .comment-box img {
        width: 50px;
        height: 50px;
    }
    .comment-box .media-left {
        padding-right: 10px;
        width: 65px;
    }
    .comment-box .media-body p {
        border: 1px solid #ddd;
        padding: 5px;
    }
    .comment-box .media-body .media p {
        margin-bottom: 0;
    }
    .comment-box .media-heading {
        background-color: #f5f5f5;
        border: 1px solid #ddd;
        padding: 7px 10px;
        position: relative;
        margin-bottom: -1px;
    }
    .comment-box .media-heading:before {
        content: "";
        width: 12px;
        height: 12px;
        background-color: #f5f5f5;
        border: 1px solid #ddd;
        border-width: 1px 0 0 1px;
        -webkit-transform: rotate(-45deg);
        transform: rotate(-45deg);
        position: absolute;
        top: 10px;
        left: -6px;
    }
    
    </style>

</head>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <div class="logo">
                        <a href="home.php">RP System</a>
                    </div>
                </header>

                <div class="container mt-5">


                    <!--Section: Content-->
                    <section class="">

                        <!-- Grid row -->
                        <div class="row">


                        <?php
                        while ($result = mysqli_fetch_assoc($query)) {
                        ?>
                        

                            <!-- Grid column -->
                            <div class="col-lg-5 col-md-12 mb-lg-0 mb-4">

                                <!-- Featured image -->
                                <div class="view overlay rounded z-depth-1 mb-lg-0 mb-4">
                                    <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/52.jpg" alt="Sample image">
                                    <a>
                                        <div class="mask rgba-white-slight"></div>
                                    </a>
                                </div>

                            </div>
                            <!-- Grid column -->

                            <!-- Grid column -->
                            <div class="col-lg-7 col-md-6 mb-md-0 mb-4 mt-xl-4">

                                <h3 class="font-weight-normal"><?php echo $result['subjectid']; ?></h3>
                                <p><?php echo $result['thname']; ?>
                                <br><?php echo $result['enname']; ?></p>
                                <div class="text-muted"><?php echo $result['detail']; ?></div>

                            </div>
                            <!-- Grid column -->
                        <?php
                        }

                        ?>

                        </div>
                        <!-- Grid row -->

                    </section>
                    <!--Section: Content-->


                </div>

                <!-- Contact Form -->
                <section class="contact-form">
                    
                    <div class="container">
                        
                        <h4>Comment</h4>
                        <br>
                        
                                <form class="form-group" method="POST" action="subjectdatail.php?id=<?php echo $id?>">
                                    <input type="text" id="main-commen" name="main-commen" placeholder="Please to comment" class="f" row="10">
                                    <input type="submit" id="btnSave" name="btnSave" class="btn-warning" value="Submit" onClick="document.location.reload(true)">
                                </form>

                        <?php 
                            while ($result_comment = mysqli_fetch_assoc($query_comments)) {
                        ?>

                        <div class="media comment-box">
                                <div class="media-left">
                                    <a href="#">
                                        <img class="img-responsive user-photo" src="images/avatar2.png">
                                    </a>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading"><?php echo $result_comment['firstname'] ?></h4>
                                    <p><?php echo $result_comment['txt'] ?></p>
                                    
                                </div>
                        </div>


                        

                        <?php
                            }
                        ?>

                        

                    </div>
                </section>

            </div>
        </div>



        <!-- Sidebar -->
        <div id="sidebar">

            <div class="inner">

                <!-- Search Box -->
                <section id="search" class="alt">
                    <form method="get" action="#">
                        <input type="text" name="search" id="search" placeholder="Search..." />
                    </form>
                </section>

                <!-- Menu -->
                <nav id="menu">
                    <ul>
                        <li><a href="home.php">HOME</a></li>

                        <li><a href="planing-before.php">Plan registration</a></li>

                        <li>
                            <span class="opener">Course</span>
                            <ul>
                                <li><a href="course-mkt.php">Marketing</a></li>
                                <li><a href="course-fin.php">Finance</a></li>
                                <li><a href="course-hrm.php">Human Resource Management</a></li>
                                <li><a href="course-bis.php">Business Information System</a></li>
                                <li><a href="course-lm.php">Logistics Management</a></li>
                                <li><a href="course-mice.php">Meeting Incentive Convention and Exhibition Management</a></li>
                                <li><a href="course-acc.php">Accounting</a></li>
                            </ul>
                        </li>

                        <li>
                            <span class="opener">Subject Category</span>
                            <ul>
                                <li><a href="subjectc-eng.php">English Subject</a></li>
                                <li><a href="subjectc-minor.php">Minor Subject</a></li>
                                <li><a href="subjectc-free.php">Free Subject</a></li>
                                <li><a href="subjectc-aas.php">Aesthetics and sports</a></li>
                                <li><a href="subjectc-main.php">Major Subject</a></li>
                            </ul>
                        </li>

                        <li><a href="profile.php">Profile</a></li>

                        <li><a href="logout.php">Logout</a></li>

                    </ul>
                </nav>

                <!-- Featured Posts -->
                <div class="featured-posts">
                    <div class="heading">
                        <h2>Developer</h2>
                    </div>
                    <div class="owl-carousel owl-theme">
                        <div class="featured-item">
                            <img src="pic-admin/sho.jpg" alt="featured one">
                            <p>CHANATIP SRISAI 6010513003</p>
                        </div>
                        </a>
                        <div class="featured-item">
                            <img src="pic-admin/frame.jpg" alt="featured two">
                            <p>KORAWIT BORAPAK 6010513039</p>
                        </div>
                        </a>
                        <div class="featured-item">
                            <img src="pic-admin/peat.jpg" alt="featured three">
                            <p>CHAYANON BOONCHUAI 6010513043</p>
                        </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Scripts -->
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/transition.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/custom.js"></script>
    <script src="assets/js/scripts.js"></script>
</body>

</html>