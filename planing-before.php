<?php 
    session_start();
    include('connect.php'); 
    if (!isset($_SESSION["studentid"])) {
        header("location:login.php");
    }

    $sql = "SELECT * FROM major";
    $query = mysqli_query($connect,$sql);
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>RP System Planning</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/bootstrap/css/bootstrap.form.css" rel="stylesheet">

    <!--
Ramayana CSS Template
https://templatemo.com/tm-529-ramayana
-->

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-style.css">
    <link rel="stylesheet" href="assets/css/owl.css">



</head>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <div class="logo">
                        <a href="home.php">RP SYSTEM</a>
                    </div>
                </header>

                <form id="planing" action="planing-after.php" method="post">
                    <div class="w3-container w3-card-4">

                        <div class="w3-container w3-blue">
                            <h1>การวางแผนการลงทะเบียนและคาดการณ์เกรดเฉลี่ย</h1>
                        </div>
                            <br>
                            <label class="w3-text"><b>หลักสูตร/สาขา</b></label>
                            <select class="w3-select w3-border" name="major">

                                <?php
                                while ($result = mysqli_fetch_assoc($query)) {
                                ?>
                                <option value="<?php echo $result['major_sh'] ?>"> <?php echo $result['major'] ?></option>
                                <?php
                                }
                                ?>
                                
                            </select>
                            <br>

                            <br>
                            <label class="w3-text"><b>ชั้นปี</b></label>
                            <select class="w3-select w3-border" name="year" id="year" require>
                                <option value="" disabled selected>Choose your option</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                            <br>

                            <br>
                            <label class="w3-text"><b>ภาคการศึกษาที่</b></label>
                            <select class="w3-select w3-border" name="semester" id="semester" require>
                                <option value="" disabled selected>Choose your option</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                            </select>
                            <br>
                            <br>
                            <br>
                    </div>
                </form>
                            <right><button class="w3-button w3-block w3-teal" onclick="checkValue()">ยืนยัน</button></right></p>

            </div>
        </div>

        <!-- Sidebar -->
        <div id="sidebar">

            <div class="inner">

                <!-- Search Box -->
                <section id="search" class="alt">
                    <form method="get" action="#">
                        <input type="text" name="search" id="search" placeholder="Search..." />
                    </form>
                </section>

                <!-- Menu -->
                <nav id="menu">
                    <ul>
                        <li><a href="home.php">HOME</a></li>

                        <li><a href="planing-before.php">Plan registration</a></li>

                        <li>
                            <span class="opener">Course</span>
                            <ul>
                                <li><a href="course-mkt.php">Marketing</a></li>
                                <li><a href="course-fin.php">Finance</a></li>
                                <li><a href="course-hrm.php">Human Resource Management</a></li>
                                <li><a href="course-bis.php">Business Information System</a></li>
                                <li><a href="course-lm.php">Logistics Management</a></li>
                                <li><a href="course-mice.php">Meeting Incentive Convention and Exhibition Management</a></li>
                                <li><a href="course-acc.php">Accounting</a></li>
                            </ul>
                        </li>

                        <li>
                            <span class="opener">Subject Category</span>
                            <ul>
                                <li><a href="subjectc-eng.php">English Subject</a></li>
                                <li><a href="subjectc-minor.php">Minor Subject</a></li>
                                <li><a href="subjectc-free.php">Free Subject</a></li>
                                <li><a href="subjectc-aas.php">Aesthetics and sports</a></li>
                                <li><a href="subjectc-main.php">Major Subject</a></li>
                            </ul>
                        </li>

                        <li><a href="profile.php">Profile</a></li>

                        <li><a href="logout.php">Logout</a></li>

                    </ul>
                </nav>

                <!-- Featured Posts -->
                <div class="featured-posts">
                    <div class="heading">
                        <h2>Developer</h2>
                    </div>
                    <div class="owl-carousel owl-theme">
                        <div class="featured-item">
                            <img src="pic-admin/sho.jpg" alt="featured one">
                            <p>CHANATIP SRISAI 6010513003</p>
                        </div>
                        </a>
                        <div class="featured-item">
                            <img src="pic-admin/frame.jpg" alt="featured two">
                            <p>KORAWIT BORAPAK 6010513039</p>
                        </div>
                        </a>
                        <div class="featured-item">
                            <img src="pic-admin/peat.jpg" alt="featured three">
                            <p>CHAYANON BOONCHUAI 6010513043</p>
                        </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Scripts -->
    <script>
        function checkValue() {
            year = document.getElementById("year").value
            semester = document.getElementById("semester").value
            if (year == '' || semester == '') {
                alert("Please fill year and semester.");
            }else{
                document.getElementById("planing").submit();
            }
        }
    </script>
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/transition.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/custom.js"></script>
</body>
</html>