<?php 
    session_start();
    include('connect.php');
    if (!isset($_SESSION["studentid"])) {
        header("location:login.php");
    }
    

    $major = $_POST['major'];
    $year = $_POST['year'];
    $semester = $_POST['semester'];

    $sql = "SELECT * FROM subjects, semesters 
                WHERE (subjects.subjectid = semesters.subjectsid) 
                AND (semesters.majorsid= '$major' or semesters.majorsid= 'all' ) 
                AND (semesters.semesterss = '$semester' or semesters.semesterss= 'all' ) 
                AND semesters.yearss= '$year'";
    // $sql = "SELECT * FROM semesters WHERE (majorsid= '$major' or majorsid= 'all' ) and (semesterss = '$semester' or semesterss= 'all' ) and yearss= '$year' ";
    mysqli_set_charset($connect,'utf8');
    $query = mysqli_query($connect,$sql);
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>RP System Planning</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css">
    <link rel="stylesheet" href="assets/css/templatemo-style.css">
    <link rel="stylesheet" href="assets/css/owl.css">

</head>

<body class="is-preload">

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Main -->
        <div id="main">
            <div class="inner">

                <!-- Header -->
                <header id="header">
                    <div class="logo">
                        <a href="home.php">RP System</a>
                    </div>
                </header>

            

                <!-- Banner -->
                <section class="main-banner">
                    <div class="wrapper">
                        <h1>การวางแผนการลงทะเบียนและคาดการณ์เกรดเฉลี่ย</h1>
                        <div class="form-row">
                            <div class="col-5.5">
                                <select id="course-code" class="form-control">
                                    <?php
                                    while ($result = mysqli_fetch_assoc($query)) {
                                    ?>
                                    <option value="<?php echo $result['subjectsid']. ' ' .$result['thname']. '+' .$result['credit'] ?>"> <?php echo $result['subjectsid']. ' ' .$result['thname'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                
                            </div>
                            
                            <div class="col-2">
                                <select id="grade" class="form-control">
                                    <option value="">เกรด</option>
                                    <option value="4">A</option>
                                    <option value="3.5">B+</option>
                                    <option value="3">B</option>
                                    <option value="2.5">C+</option>
                                    <option value="2">C</option>
                                    <option value="1.5">D+</option>
                                    <option value="1">D</option>
                                    <option value="0">E</option>
                                </select>
                            </div>
                            <div class="col-2">
                                <input id="add" class="btn btn-primary btn-block" type="button" value="Add">

                            </div>
                        </div>
                        <table id="table" class="table table-bordered display-none">
                            <thead>
                                <tr>
                                    <th>รหัสวิชา/ชื่อวิชา</th>
                                    <th>หน่วยกิต</th>
                                    <th>เกรด</th>
                                </tr>
                            </thead>
                            <tbody id="tbody">

                            </tbody>
                            <tfoot id="tfoot">

                            </tfoot>
                        </table>
                        <div class="row">
                            <div class="col">
                                <input id="calc-gp" class="btn btn-success display-none" type="button" value="คำนวณGPA">
                            </div>
                            <div class="col">
                                <input id="clear" class="btn btn-warning display-none" type="button" value="ล้าง">
                            </div>
                        </div>
                    </div>
                    <script src="main.js"></script>
                </section>


                <!-- Services -->

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="service-item first-item">

                            </div>
                        </div>
                    </div>
                </div>
                </section>

            </div>
        </div>

        <!-- Sidebar -->
        <div id="sidebar">

            <div class="inner">

                <!-- Search Box -->
                <section id="search" class="alt">
                    <form method="get" action="#">
                        <input type="text" name="search" id="search" placeholder="Search..." />
                    </form>
                </section>

                <!-- Menu -->
                <nav id="menu">
                    <ul>
                        <li><a href="home.php">HOME</a></li>

                        <li><a href="planing-before.php">Plan registration</a></li>

                        <li>
                            <span class="opener">Course</span>
                            <ul>
                                <li><a href="course-mkt.php">Marketing</a></li>
                                <li><a href="course-fin.php">Finance</a></li>
                                <li><a href="course-hrm.php">Human Resource Management</a></li>
                                <li><a href="course-bis.php">Business Information System</a></li>
                                <li><a href="course-lm.php">Logistics Management</a></li>
                                <li><a href="course-mice.php">Meeting Incentive Convention and Exhibition Management</a></li>
                                <li><a href="course-acc.php">Accounting</a></li>
                            </ul>
                        </li>

                        <li>
                            <span class="opener">Subject Category</span>
                            <ul>
                                <li><a href="subjectc-eng.php">English Subject</a></li>
                                <li><a href="subjectc-minor.php">Minor Subject</a></li>
                                <li><a href="subjectc-free.php">Free Subject</a></li>
                                <li><a href="subjectc-aas.php">Aesthetics and sports</a></li>
                                <li><a href="subjectc-main.php">Major Subject</a></li>
                            </ul>
                        </li>

                        <li><a href="profile.php">Profile</a></li>

                        <li><a href="logout.php">Logout</a></li>

                    </ul>
                </nav>

                <!-- Featured Posts -->
                <div class="featured-posts">
                    <div class="heading">
                        <h2>Developer</h2>
                    </div>
                    <div class="owl-carousel owl-theme">
                        <div class="featured-item">
                            <img src="pic-admin/sho.jpg" alt="featured one">
                            <p>CHANATIP SRISAI 6010513003</p>
                        </div>
                        </a>
                        <div class="featured-item">
                            <img src="pic-admin/frame.jpg" alt="featured two">
                            <p>KORAWIT BORAPAK 6010513039</p>
                        </div>
                        </a>
                        <div class="featured-item">
                            <img src="pic-admin/peat.jpg" alt="featured three">
                            <p>CHAYANON BOONCHUAI 6010513043</p>
                        </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Scripts -->
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="assets/js/browser.min.js"></script>
    <script src="assets/js/breakpoints.min.js"></script>
    <script src="assets/js/transition.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/custom.js"></script>
</body>



</html>